## :dart: About ##
The chatbot provides the latest information on the COVID-19 outbreak by consulting regular updates from the WHO website and national and local public health authorities where you live.

It is based on the RASA system, is rule-delimited and calibrated for the Spanish language. 


## :sparkles: Features ##

- :heavy_check_mark:The Chatbot acquires the information from the WHO, in the FAQ section. [FAQ](https://www.who.int/es/emergencies/diseases/novel-coronavirus-2019/advice-for-public/q-a-coronaviruses);
- :heavy_check_mark: The questions and answers are in Spanish;
- :heavy_check_mark: The questions do not provide specific information on vaccines.;

## :rocket: Technologies ##

The following tools were used in this project:

- [Rasa Chabot](https://rasa.com/)
- [Rasa Chabot Installation](https://rasa.com/docs/rasa/installation)

## :white_check_mark: Requirements ##

Before starting :checkered_flag:, you need to have [Git](https://git-scm.com) and [python](https://www.python.org/downloads/windows/) installed.

## :checkered_flag: Starting ##

```bash
# Clone this project
$ git clone https://github.com/jonathanm31/chatbot

# Access
$ cd chatbot

# Run the project
$ rasa init

# The server will initialize in the <http://localhost:3000>
```
